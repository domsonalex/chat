import React from 'react'

export default function Modal ({ show, title, onCancel, onSubmit, submitText, cancelText = 'Cancel' }) {
  if (!show) return null
  return (
    <div className="modal opened">
      <div className="modal-inner">
        <div className='modal-wrap'>
          <div className="modal-header">
            <div className="modal-title">
              { title }
            </div>
            <div className="modal-close" onClick={() => onCancel()}>
              <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"/></svg>
            </div>
          </div>
          <div className="modal-body"></div>
          <div className="modal-footer">
            <button className="button" onClick={() => onSubmit()}>{submitText}</button>
            <button className="button" onClick={() => onCancel()}>{cancelText}</button>
          </div>
        </div>
      </div>
    </div>
  )
}
