import React from 'react'
import classNames from 'classnames'
import { getReplaceMessage } from '../selectors/room'

export default function Message ({ message, users }) {
  function createMarkup () {
    return { __html: getReplaceMessage(message.message) }
  }
  function MyComponent () {
    return <p className="message-text-wrapper" dangerouslySetInnerHTML={createMarkup()} />
  }
  return (
    <div className={classNames('message-bubble', { me: message.user_id === users.me.id })}>
      <div className="message-bubble-inner">
        <div className="message-avatar">
          <img src={users[message.user_id].logo} alt="logo" />
        </div>
        <div className="message-text">
          {MyComponent()}
          { message.user_id === users.me.id && message.read === 1
            ? <svg className="message-checked" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g id="Layer_2" data-name="Layer 2"><g id="checkmark"><g id="checkmark-2" data-name="checkmark"><rect className="cl-1" width="24" height="24" fill='#3D00F1' /><path fill='#fff' d="M9.86,18a1,1,0,0,1-.73-.32L4.27,12.51a1,1,0,1,1,1.46-1.37l4.12,4.39,8.41-9.2a1,1,0,1,1,1.48,1.34l-9.14,10a1,1,0,0,1-.73.33Z"/></g></g></g></svg>
            : null
          }
        </div>
      </div>
      <div className="clearfix"></div>
    </div>
  )
}
