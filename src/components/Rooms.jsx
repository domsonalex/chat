import { useSelector, useDispatch } from 'react-redux'
import React from 'react'
import { getTimeFrom } from './../selectors/room'
import { openRoom } from '../actions/user'
import classNames from 'classnames'

export const Room = ({ room }) => {
  const dispatch = useDispatch()
  const lastMessage = room.messages[0] || {}
  const active = useSelector(({ rooms }) => rooms.active)

  return (
    <li className={classNames('a-user-box', { active: room.id === active })} onClick={() => openRoom(room.id)(dispatch)}>
      <div className="a-user-avatar">
        <img src={room.passiveUser.logo} alt="logo" />
      </div>
      <div className="a-user-by">
        <div className="a-user-by-headline">
          <h5>
            {room.passiveUser.full_name}
          </h5>
          <span>
            {getTimeFrom(lastMessage.updated_at || room.created_at)}
          </span>
        </div>
        <div className="a-user-by-content">
          <p>
            {lastMessage.message}
          </p>
          {room.unreadedMessages !== 0 &&
            <div className="message-unread">
              {room.unreadedMessages}
            </div>
          }
        </div>
      </div>
    </li>
  )
}

export default function Rooms () {
  const rooms = useSelector(state => state.rooms.list)
  return (
    <ul>
      {rooms.map(room => (
        <Room key={room.id} room={room} />
      ))}
    </ul>
  )
}
