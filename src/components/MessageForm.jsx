import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { sendMessage } from '../actions/send'

export default function MessageForm () {
  const [msg, setMsg] = useState('')
  const active = useSelector(({ rooms }) => rooms.active)
  const sending = () => {
    sendMessage(active, msg)
    setMsg('')
  }
  const sendingKey = (e) => {
    if ((e.ctrlKey && e.key === 'Enter') || (e.metaKey && e.key === 'Enter')) {
      e.preventDefault()
      sendMessage(active, msg)
      setMsg('')
    }
  }
  return (
    <div className="message-reply">
      <textarea onKeyUp={sendingKey} onChange={(e) => setMsg(e.target.value)} cols="1" rows="1" placeholder="Your Message" value={msg}></textarea>
      <button className="button ripple-effect" onClick={sending}>Send</button>
    </div>
  )
}
