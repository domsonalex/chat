import React, { Fragment, useState, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getGroupedMessages, getUsers } from '../selectors/room'
import moment from 'moment'
import Message from './Message'
import Modal from './Modal'
import MessageForm from './MessageForm'
import { readMessages, loadMessages } from '../actions/send'
import { removeRoom } from '../actions/user'

var isVisible = function (target) {
  if (!target) return false
  var targetPosition = {
    top: window.pageYOffset + target.getBoundingClientRect().top,
    bottom: window.pageYOffset + target.getBoundingClientRect().bottom
  }
  var windowPosition = {
    top: window.pageYOffset + 220,
    bottom: window.pageYOffset + document.documentElement.clientHeight
  }
  if (targetPosition.bottom > windowPosition.top &&
      targetPosition.bottom > 0 &&
      targetPosition.top > 0 &&
      targetPosition.top < windowPosition.bottom) {
    return true
  } else {
    return false
  }
}

export default function Conversation (props) {
  const dispatch = useDispatch()
  const groupedMessages = useSelector(getGroupedMessages)
  const users = useSelector(getUsers)
  const [showModal, setShowModal] = useState(false)
  const [page, setPage] = useState(2)
  const active = useSelector(({ rooms }) => rooms.active)
  const list = useSelector(({ rooms }) => rooms.list)
  let refs = []
  const clientConversation = useRef(null)

  const onScrroll = () => {
    if (clientConversation.current.scrollTop === 0) {
      console.log(clientConversation.current.scrollTop, page, list.find(el => el.id === active).pages)
      if (page < list.find(el => el.id === active).pages) {
        loadMessages(active, page)(dispatch)
        setPage((page) => page + 1)
      }
    }
    readUnreaded()
  }

  const readUnreaded = () => {
    const visibleUnreadedMsgs = refs.filter(el => isVisible(el.ref))
    if (!visibleUnreadedMsgs.length) return
    readMessages(visibleUnreadedMsgs.map(el => el.msg))
    refs = refs.filter(el => !visibleUnreadedMsgs.includes(el))
  }

  const deleteRoom = () => {
    removeRoom(active)
    setShowModal(false)
  }

  useEffect(readUnreaded)

  const addRef = (ref, msg) => {
    if (msg.read === 0 && msg.user_id !== users.me.id) {
      refs.push({ ref, msg })
    }
  }

  if (!users) return null
  return (
    <>
      <div className="message-content">
        <div className="messages-headline">
          <span className="message-back">
            <svg height="48" viewBox="0 0 48 48" width="48" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h48v48h-48z" fill="none"/><path d="M40 22h-24.34l11.17-11.17-2.83-2.83-16 16 16 16 2.83-2.83-11.17-11.17h24.34v-4z"/></svg>
          </span>
          <h4>
            {users.he.full_name}
            <span className="message-action" onClick={() => setShowModal(true)}><i className="icon-feather-trash-2"></i> Delete Conversation</span>
          </h4>
        </div>
        <div onScroll={onScrroll} ref={clientConversation} className="message-content-inner" >
          {
            groupedMessages.map(messages => {
              return (
                <Fragment key={messages[0].id}>
                  {
                    messages.map(message => {
                      return (
                        <div key={message.id} ref={(ref) => addRef(ref, message)}>
                          <Message message={message} users={users} />
                        </div>
                      )
                    })
                  }
                  <div className="message-time-sign">
                    <span>
                      {moment(messages[0].created_at.split(' ')[0]).format('DD MMMM, YYYY')}
                    </span>
                  </div>
                </Fragment>
              )
            })
          }
        </div>
        <MessageForm />
      </div>
      <Modal
        show={showModal}
        onSubmit={deleteRoom}
        onCancel={() => setShowModal(false)}
        title='Delete conversation'
        submitText='Delete'
      />
    </>
  )
}
