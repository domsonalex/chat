import React from 'react'
import { Provider } from 'react-redux'
import store from './reducers'
import ChatContainer from './containers/ChatContainer'

export default function App () {
  return (
    <Provider store={store}>
      <ChatContainer />
    </Provider>
  )
}
