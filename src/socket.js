import Echo from 'laravel-echo'
import store from './reducers'
import { fetchRooms } from './actions/rooms'

window.Pusher = require('pusher-js')

window.Echo = new Echo({
  broadcaster: 'pusher',
  key: process.env.MIX_PUSHER_APP_KEY,
  wsHost: window.location.hostname,
  wsPort: 6001,
  wssPort: 6001,
  disableStats: true,
  encrypted: process.env.PUSHER_APP_ENCRYPTED
})

const listenMessages = (dispatch) => {
  window.Echo.join('ChatChannel')
    .listen('\\App\\Events\\Chat\\NewMessageEvent', ({ message }) => {
      const { rooms } = store.getState()
      if (!rooms.list.find(el => el.id === message.room_id)) {
        fetchRooms()(dispatch)
      } else {
        dispatch({
          type: 'ADD_MESSAGE',
          message
        })
      }
    })
    .listen('\\App\\Events\\Chat\\ReadedMessagesEvent', ({ messages }) => {
      dispatch({
        type: 'READ_MESSAGE',
        messages
      })
    })
    .listen('\\App\\Events\\DeletedRoomEvent', ({ roomId }) => {
      dispatch({
        type: 'REMOVE_ROOM',
        roomId
      })
    })
}

export default listenMessages
