import axios from 'axios'

export const openRoom = (id) => dispatch => {
  dispatch({
    type: 'SET_ACTIVE_ROOM',
    id
  })
}
export const removeRoom = (id) => {
  axios.post(`/chat/rooms/${id}/delete`)
}
