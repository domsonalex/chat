import axios from 'axios'

export const sendMessage = (roomId, message) => {
  const trimmed = message.trim()
  if (!trimmed.length) return Promise.reject(new Error('message empty'))
  return axios.post(`/chat/rooms/${roomId}/messages`, { message: trimmed })
}

export const readMessages = (msgs) => {
  axios.post(`/chat/rooms/${msgs[0].room_id}/messages/read`, { messages: msgs.map(msg => msg.id.toString()) })
}

export const loadMessages = (roomId, page) => dispatch => {
  axios.get(`/chat/rooms/${roomId}/messages?page=${page}`)
    .then(({ data }) => {
      dispatch({
        type: 'LOAD_MESSAGES',
        payload: data.response.messages
      })
    })
}
