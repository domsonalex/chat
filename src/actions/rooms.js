import axios from 'axios'

export const fetchRooms = () => dispatch => {
  axios.get('/chat/rooms')
    .then(({ data }) => {
      dispatch({
        type: 'SET_ROOMS',
        payload: data.response.userRooms
      })
      dispatch({
        type: 'SET_USER_DATA',
        payload: data.response.activeUser
      })
      const id = Number(document.getElementById('chat').getAttribute(':activechat'))
      if (id) {
        dispatch({
          type: 'SET_ACTIVE_ROOM',
          id
        })
      }
    })
}
