import { connect } from 'react-redux'
import Chat from '../components/Chat'

const mapStateToProps = ({ rooms } /*, ownProps */) => {
  return {
    rooms
  }
}

const mapDispatchToProps = { }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat)
