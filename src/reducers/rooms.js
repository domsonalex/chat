const initial = {
  list: [],
  active: '',
  user: {
    id: '',
    full_name: '',
    logo: ''
  }
}

const reducers = {
  SET_USER_DATA: (state, { payload }) => {
    return {
      ...state,
      user: payload
    }
  },
  SET_ROOMS: (state, action) => {
    return {
      ...state,
      list: action.payload
    }
  },
  SET_ACTIVE_ROOM: (state, { id }) => ({
    ...state,
    active: id
  }),
  REMOVE_ROOM: (state, { roomId }) => {
    return {
      ...state,
      active: state.active === roomId ? '' : state.active,
      list: state.list.filter(room => room.id !== roomId)
    }
  },
  ADD_MESSAGE: (state, { message }) => {
    const index = state.list.findIndex(room => room.id === message.room_id)
    const list = [...state.list]
    const room = list[index]
    list.splice(index, 1)
    const msgs = [message, ...room.messages]
    return {
      ...state,
      list: [{
        ...room,
        unreadedMessages: msgs.filter(el => el.read === 0 && el.user_id !== state.user.id).length,
        messages: msgs
      }, ...list]
    }
  },
  READ_MESSAGE: (state, { messages }) => {
    const roomId = messages[0].room_id
    const index = state.list.findIndex(room => room.id === roomId)
    const list = [...state.list]
    const room = { ...list[index], messages: [...list[index].messages] }
    messages.forEach(msg => {
      const msgIndex = room.messages.findIndex(message => message.id === msg.id)
      room.messages.splice(msgIndex, 1, msg)
    })
    list.splice(index, 1, {
      ...room,
      unreadedMessages: room.messages.filter(el => el.read === 0 && el.user_id !== state.user.id).length
    })
    return {
      ...state,
      list
    }
  },
  LOAD_MESSAGES: (state, { payload }) => {
    const lastMsg = Object.values(payload)[0]
    const index = state.list.findIndex(room => room.id === lastMsg.room_id)
    const list = [...state.list]
    const room = list[index]
    const msgs = [...room.messages, ...Object.values(payload)]
    room.messages = [...msgs]
    list.slice(index, 1, room)
    return {
      ...state,
      list
    }
  }
}

export default (state = initial, action) => {
  const reducer = reducers[action.type]
  return reducer ? reducer(state, action) : state
}
