import { createStore, combineReducers, applyMiddleware } from 'redux'
import rooms from './rooms'
import thunk from 'redux-thunk'

const store = createStore(combineReducers({
  rooms
}), applyMiddleware(thunk))

export default store

store.subscribe(() => console.log(store.getState()))
